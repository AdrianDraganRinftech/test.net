﻿using Test.D1.Api.Dto;
using System.Collections.Generic;

namespace Test.D1.Api.Dal
{
    public interface IProductsService
    {
        IEnumerable<ProductDto> GetAll();
        ProductDto Add(NewProductDto newProduct);
        ProductDto Delete(string id);
    }
}
