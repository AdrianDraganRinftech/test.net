﻿using Test.D2.Web.Models;
using System.Collections.Generic;

namespace Test.D2.Web.Services
{
    public interface IProductService
    {
        IEnumerable<ProductModel> GetAll();
        ProductModel Add(NewProductModel newProduct);
    }
}